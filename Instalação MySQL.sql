/* Usuário */
create table usuario(
id_usuario int not null primary key auto_increment,
nome varchar(500) not null,
email varchar(100) not null unique,
senha varchar(500) not null,
token_externo varchar(500),
id_externo varchar(500),
url_foto varchar(500),
ativo varchar(1) not null,
data_criacao datetime default current_timestamp
);

/* Livros */
create table livro(
id_livro int not null primary key auto_increment,
titulo varchar(250) not null,
subtitulo varchar(250),
autor text not null,
descricao text,
editora varchar(250),
isbn text,
numero_paginas int null,
ano_publicacao varchar(5),
categorias text,
idioma varchar(50),
smallimage text,
mediumimage text,
largeimage text,
extralargeimage text,
smallthumbnail text,
thumbnail text, 
data_criacao datetime default current_timestamp
);

create table livroUsuario(
id_livroUsuario int not null primary key auto_increment,
id_livro int not null,
id_usuario int not null,
data_criacao datetime default current_timestamp,
status_leitura varchar(10)
);
alter table livroUsuario add constraint fk_livroUsuario_id_usuario
foreign key (id_usuario) references usuario(id_usuario);

alter table livroUsuario add constraint fk_livroUsuario_id_livro
foreign key (id_livro) references livro(id_livro);

create index livroUsuario_UK_livro on livroUsuario (id_livro, id_usuario)

/* NOTAS */
create table nota(
id_notas int not null primary key auto_increment,
id_livro int not null,
id_usuario int not null,
titulo text not null,
cor text not null,
texto text not null,
pagina int not null,
tag varchar(50),
data_criacao datetime default current_timestamp
);
alter table nota add constraint fk_nota_id_livro
foreign key (id_livro) references livro(id_livro);

alter table nota add constraint fk_nota_id_usuario
foreign key (id_usuario) references usuario(id_usuario);


