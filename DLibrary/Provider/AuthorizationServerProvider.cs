﻿using DLibrary.Data.Models;
using DLibrary.Data.Repository;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DLibrary.Provider
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
	{
		public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
		{
			context.Validated();
		}

		//aqui é se não achar o token do usuario ou não existir
		public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
		{
			//context.OwinContext.Response.Headers.Add("Access-control-allow-origin", new[] { "*" });

			try
			{
                using (RepositoryBase<Usuario> _repo = new RepositoryBase<Usuario>())
                {
                    var user = _repo.Get(d => d.Email == context.UserName && d.Password == context.Password).FirstOrDefault();

                    if (user == null)
                    {
                        context.SetError("invalid_grant", "Usuário ou senha incorretos.");
                        return;
                    }
                }

                //authenticationtype é o tipo no nosso caso bearer token
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
				identity.AddClaim(new Claim("sub", context.UserName));
				identity.AddClaim(new Claim("role", "user"));

				//a geração acontece aqui por debaixo dos panos
				context.Validated(identity);
			}
			catch (Exception)
			{
				context.SetError("invalid_grant", "Falha ao atenticar");
			}
		}
	}
}