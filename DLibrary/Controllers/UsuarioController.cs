﻿using AutoMapper;
using DLibrary.Data.Models;
using DLibrary.Data.Repository;
using DLibrary.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DLibrary.Controllers
{
    [RoutePrefix("api/Usuario")]
    public class UsuarioController : ApiController
    {
        private readonly RepositoryBase<Usuario> _repo;

        public UsuarioController()
        {
            _repo = new RepositoryBase<Usuario>();
        }

        [Route("Register")]
        public async Task<IHttpActionResult> Register(UsuarioRequest usuario)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (usuario.Password == string.Empty)
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }

                var user = Mapper.Map<UsuarioRequest, Usuario>(usuario);
                _repo.Add(user);
                _repo.Save();

                return Ok("Usuário cadastrado com sucesso.");
            }
            catch (System.Exception e)
            {
                return Json(e.InnerException.InnerException.Message);
            }
        }

        [Route("Login")]
        public IHttpActionResult Login(UsuarioRequest usuario)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                Usuario usuarioExistente = new Usuario();

                //login externo
                if (!string.IsNullOrEmpty(usuario.IdExterno))
                {
                    usuarioExistente = _repo.Get(d => d.Email == usuario.Email && d.IdExterno == usuario.IdExterno).FirstOrDefault();

                    if (usuarioExistente == null)
                    {
                        usuario.Password = usuario.TokenExterno;
                        var user = Mapper.Map<UsuarioRequest, Usuario>(usuario);
                        _repo.Add(user);
                        _repo.Save();

                        usuarioExistente = user;
                    }

                }
                //login convencional
                else if (!string.IsNullOrEmpty(usuario.Email) && !string.IsNullOrEmpty(usuario.Password))
                {
                    usuarioExistente = _repo.Get(d => d.Email == usuario.Email && d.Password == usuario.Password).FirstOrDefault();
                }
                else
                {
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
                }

                if(usuarioExistente == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);

                string baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority) + Configuration.VirtualPathRoot;
                using (var httpClient = new HttpClient())
                {
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username", usuarioExistente.Email),
                        new KeyValuePair<string, string>("Password", usuarioExistente.Password)
                    });

                    HttpResponseMessage result = httpClient.PostAsync(baseUrl + "Login", content).Result;

                    string resultContent = result.Content.ReadAsStringAsync().Result;

                    JObject json = JObject.Parse(resultContent);
                    json.Add("Id_usuario", usuarioExistente.Id);

                    return Ok(json);
                }
            }
            catch (System.Exception e)
            {
                return Json(e.InnerException.InnerException.Message);
            }
        }
    }
}
