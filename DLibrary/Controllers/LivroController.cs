﻿using AutoMapper;
using DLibrary.Data.Repository;
using DLibrary.Helpers;
using DLibrary.Model.Models;
using DLibrary.Models;
using Google.Apis.Books.v1;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DLibrary.Controllers
{
    //[Authorize]
    [RoutePrefix("api/Livro")]
    public class LivroController : ApiController
    {
        private readonly RepositoryBase<Livro> _repo;
        private readonly RepositoryBase<LivroUsuario> _repoLivroUsuario;

        public LivroController()
        {
            _repo = new RepositoryBase<Livro>();
            _repoLivroUsuario = new RepositoryBase<LivroUsuario>();
        }

        [Route("Buscar")]
        [HttpGet]
        public IList<LivroResponse> Get(String data = null, int skip = 0, int take = 10)
        {
            var lista = new List<Livro>();
            var filtroPrincipal = MakeFilter.Do<Livro>("Livro", data, false);
            var filtroLivrosUsuario = MakeFilter.Do<LivroUsuario>("LivroUsuario", data, true);

            var listaLivroUsuario = _repoLivroUsuario.Get().Where(filtroLivrosUsuario).ToList();
            var listaLivroUsuariosIds = listaLivroUsuario.Select(s => s.IdLivro).ToList();

            if (filtroPrincipal != null)
            {
                lista = _repo.Get().Where(filtroPrincipal).Where(w => listaLivroUsuariosIds.Contains(w.Id))
                    .OrderBy(s => s.Id).Skip(skip).Take(take).ToList();
            }
            else
            {
                lista = _repo.Get().Where(w => listaLivroUsuariosIds.Contains(w.Id))
                    .OrderBy(s => s.Id).Skip(skip).Take(take).ToList();
            }

            var dados = Mapper.Map<List<Livro>, List<LivroResponse>>(lista.ToList());

            foreach (var i in dados)
                i.StatusLeitura = listaLivroUsuario.Where(e => e.IdLivro == i.Id).FirstOrDefault().StatusLeitura;
                    
            return dados;
        }

        [HttpGet]
        [Route("GetByGoogle")]
        public IEnumerable<LivroResponse> GetByGoogle(string paramSearch = "", int skip = 0, int take = 4)
        {
            //978-8533613379 existe

            //9788373191723 não existe
            try
            {
                var lista = new List<LivroResponse>();
                var listaGoogle = new List<LivroResponse>();

                paramSearch = paramSearch.Replace("-", "");

                #region nosso banco
                var data = "[{Field: 'Titulo,Subtitulo,Autor,Categorias,Isbn,Editora', Operator: 'contains', Value: '" + paramSearch + "', Condition: 'or'}]";
                var filtro = MakeFilter.Do<Livro>("Livro", data, false);

                var livrosBd = _repo.Get().Where(filtro).OrderBy(s => s.Id).Skip(skip).Take(take).ToList();

                foreach (var i in livrosBd)
                    lista.Add(Mapper.Map<Livro, LivroResponse>(i));
                #endregion

                #region Google Books
                var service = new BooksService(new BaseClientService.Initializer
                {
                    ApplicationName = "TCC Library Project",
                    ApiKey = ConfigurationManager.AppSettings["ApiKey"],
                });

                var action = service.Volumes.List(paramSearch); //.Execute();
                action.MaxResults = take;
                action.StartIndex = skip;

                var result = action.Execute();

                if (result.Items == null)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Livro não localizado."));
                }

                foreach (var item in result.Items)
                {
                    var book = item.VolumeInfo;

                    var livro = new LivroResponse()
                    {
                        Titulo = book.Title,
                        Subtitulo = (book.Subtitle != null ? book.Subtitle : ""),
                        Autor = (book.Authors != null ? string.Join(",", book.Authors) : ""),
                        Descricao = (book.Description != null ? book.Description : ""),
                        Editora = (book.Publisher != null ? book.Publisher : ""),
                        NumeroPaginas = (book.PageCount != null ? Convert.ToInt64(book.PageCount) : 0),
                        Categorias = (book.Categories != null ? string.Join(",", book.Categories) : ""),
                        Idioma = (book.Language != null ? book.Language : ""),
                        AnoPublicacao = (String.IsNullOrEmpty(book.PublishedDate) ? "" : (book.PublishedDate.Length > 4 ? book.PublishedDate.Substring(0, 4) : book.PublishedDate))
                    };

                    if (book.ImageLinks != null)
                    {
                        livro.SmallImage = book.ImageLinks.Small == null ? "" : book.ImageLinks.Small;
                        livro.MediumImage = book.ImageLinks.Medium == null ? "" : book.ImageLinks.Medium;
                        livro.LargeImage = book.ImageLinks.Large == null ? "" : book.ImageLinks.Large;
                        livro.ExtraLargeImage = book.ImageLinks.ExtraLarge == null ? "" : book.ImageLinks.ExtraLarge;
                        livro.SmallThumbnail = book.ImageLinks.SmallThumbnail == null ? "" : book.ImageLinks.SmallThumbnail;
                        livro.Thumbnail = book.ImageLinks.Thumbnail == null ? "" : book.ImageLinks.Thumbnail;
                    }

                    if (book.IndustryIdentifiers != null)
                        livro.Isbn = string.Join(",", book.IndustryIdentifiers.Select(s => s.Identifier));
                    #endregion

                    listaGoogle.Add(livro);
                }

                return lista.Union(listaGoogle.AsEnumerable().OrderBy(s => s.Id));
            }
            catch (HttpResponseException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.NotFound, "Livro não localizado."));
            }
        }

        [Route("Editar")]
        public IHttpActionResult Put(int id, LivroRequest entity)
        {
            try
            {
                var livro = Mapper.Map<LivroRequest, Livro>(entity);
                livro.Id = id;
                _repo.Update(livro);
                _repo.Save();

                return Ok("Edição realizada com sucesso.");
            }
            catch (System.Exception e)
            {
                return Json(e.InnerException.InnerException.Message);
            }
        }

        [Route("Inserir")]
        public IHttpActionResult Post([FromBody] LivroRequest entity, Int64 IdUsuario)
        {
            try
            {
                var livro = Mapper.Map<LivroRequest, Livro>(entity);

                livro.DataCriacao = DateTime.Now;

                if (entity.Id == 0)
                {
                    _repo.Add(livro);
                    _repo.Save();
                }

                var livroUsuario = new LivroUsuario()
                {
                    IdUsuario = IdUsuario,
                    IdLivro = livro.Id,
                    StatusLeitura = entity.StatusLeitura
                };
                _repoLivroUsuario.Add(livroUsuario);
                _repoLivroUsuario.Save();

                return Ok("Livro cadastrado com sucesso.");
            }
            catch (Exception e)
            {
                return Json(e.InnerException.InnerException.Message);
            }
        }

        //[Route("Excluir")]
        //public IHttpActionResult Delete(int id)
        //{
        //    try
        //    {
        //        _repo.Delete(id);
        //        _repo.Save();

        //        return Ok("Livro excluído com sucesso.");
        //    }
        //    catch (System.Exception e)
        //    {
        //        return Json(e.InnerException.InnerException.Message);
        //    }
        //}
    }
}
