﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebApi.Helpers;

namespace DLibrary.Controllers
{
    [RoutePrefix("api/Ocr")]
    public class OcrController : ApiController
    {
        //[HttpPost]
        //[Route("Run")]
        //public IHttpActionResult Run([FromBody]string Base64Image)
        //{
        //    try
        //    {
        //        if (String.IsNullOrEmpty(Base64Image))
        //            throw new Exception("Parâmetro 'Base64Image' é obrigatório.");

        //        var ocrApi = new GoogleOcr();

        //        return Json(ocrApi.Run(Base64Image, true));
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(e.Message);
        //    }
        //}

        [HttpPost]
        [Route("api/UploadImage")]
        public async Task<IHttpActionResult> UploadImage()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();

            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        int maxcontentLength = 1024 * 1024 * 5;

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Apenas o tipos .jpg, .gif, .png são permitidos");

                            dict.Add("error", message);
                            return Json(dict);
                        }
                        else if (postedFile.ContentLength > maxcontentLength)
                        {
                            var message = string.Format("Envie imagens de até 5mb");

                            dict.Add("error", message);
                            return Json(dict);
                        }
                        else
                        {
                            var filePath = HttpContext.Current.Server.MapPath("~/Userimage/" + postedFile.FileName + extension);
                            postedFile.SaveAs(filePath);
                            var base64 = Convert.ToBase64String(File.ReadAllBytes(filePath));

                            var ocrApi = new GoogleOcr();

                            return Json(ocrApi.Run(base64, true));
                        }
                    }
                }
                var res = string.Format("Por favor escolha uma imagem.");
                dict.Add("error", res);
                return Json(dict);
            }
            catch (Exception e)
            {
                var res = string.Format("Erro no processamento da imagem: " + e.Message);
                dict.Add("error", res);
                return Json(dict);
            }
        }
    }
}