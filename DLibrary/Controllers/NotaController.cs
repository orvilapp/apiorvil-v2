﻿using AutoMapper;
using DLibrary.Data.Repository;
using DLibrary.Helpers;
using DLibrary.Model.Models;
using DLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace DLibrary.Controllers
{
    public class NotaController : ApiController
    {
        private readonly RepositoryBase<Nota> _repo;
        private readonly RepositoryBase<Livro> _repoLivros;

        public NotaController()
        {
            _repo = new RepositoryBase<Nota>();
            _repoLivros = new RepositoryBase<Livro>();
        }

        public IList<NotaResponse> Get(String data = null, int skip = 0, int take = 10)
        {
            var lista = new List<Nota>();
            var filtroPrincipal = MakeFilter.Do<Nota>("Nota", data, false);
            var filtroUsuario = MakeFilter.Do<Nota>("Nota", data, true);

            if (filtroPrincipal != null)
            {
                lista = _repo.Get().Where(filtroPrincipal).Where(filtroUsuario)
                    .OrderBy(s => s.Id).Skip(skip).Take(take).ToList();
            }
            else
                lista = _repo.Get().Where(filtroUsuario).OrderBy(s => s.Id).Skip(skip).Take(take).ToList();

            return Mapper.Map<List<Nota>, List<NotaResponse>>(lista.ToList());
        }

        public IHttpActionResult Put(int id, NotaRequest entity)
        {
            try
            {
                var Nota = Mapper.Map<NotaRequest, Nota>(entity);
              
                _repo.Update(Nota);
                _repo.Save();

                return Ok("Edição realizada com sucesso.");
            }
            catch (System.Exception e)
            {
                return Json(e.InnerException.Message);
            }
        }

        public IHttpActionResult Post([FromBody]NotaRequest entity)
        {
            try
            {
                entity.DataCriacao = DateTime.Now;

                var Nota = Mapper.Map<NotaRequest, Nota>(entity);
               
                _repo.Add(Nota);
                _repo.Save();

                return Ok("Nota gravada com sucesso");
            }
            catch (System.Exception e)
            {
                return Json(e.InnerException.InnerException.Message);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                _repo.Delete(id);
                _repo.Save();

                return Ok("Nota excluída com sucesso.");
            }
            catch (System.Exception e)
            {
                return Json(e.InnerException.InnerException.Message);
            }
        }
    }
}
