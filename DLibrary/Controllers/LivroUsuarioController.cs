﻿using DLibrary.Data.Repository;
using DLibrary.Model.Models;
using System;
using System.Linq;
using System.Transactions;
using System.Web.Http;

namespace DLibrary.Controllers
{
    [RoutePrefix("api/Livro")]
    public class LivroUsuarioController : ApiController
    {
        private readonly RepositoryBase<LivroUsuario> _repoLivroUsuario;
        private readonly RepositoryBase<Nota> _repoNota;

        public LivroUsuarioController()
        {
            _repoLivroUsuario = new RepositoryBase<LivroUsuario>();
            _repoNota = new RepositoryBase<Nota>();
        }

        public IHttpActionResult Post(LivroUsuario livroUsuario)
        {
            try
            {
                if (livroUsuario.Livro.Id == 0)
                    livroUsuario.Livro = null;

                _repoLivroUsuario.Add(livroUsuario);
                _repoLivroUsuario.Save();

                return Ok("Cadastro realizado com sucesso.");
            }
            catch (System.Exception e)
            {
                return Json(e.InnerException.InnerException.Message);
            }
        }

        public IHttpActionResult Delete(int idLivro, int idUsuario)
        {
            try
            {
                var ent = _repoLivroUsuario.Get(e => e.IdLivro == idLivro && e.IdUsuario == idUsuario).FirstOrDefault();
                var notas = _repoNota.Get(e => e.IdLivro == idLivro && e.IdUsuario == idUsuario).ToList();

                if (ent == null) throw new Exception("Entidade não encontrada");

                using (TransactionScope tran = new TransactionScope()) {

                    foreach(var i in notas)
                    {
                        _repoNota.Delete(Convert.ToInt32(i.Id));
                    }

                    _repoLivroUsuario.Delete(Convert.ToInt32(ent.Id));
                    _repoLivroUsuario.Save();
                    _repoNota.Save();
                    tran.Complete();
                }
                
                return Ok("Registro excluído com sucesso.");
            }
            catch (System.Exception e)
            {
                return Json(e.InnerException.InnerException.Message);
            }
        }

    }
}