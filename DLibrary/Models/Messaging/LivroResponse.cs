﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DLibrary.Models
{
    public class LivroResponse
    {
        public Int64 Id { get; set; }

        [Required, StringLength(250)]
        public virtual String Titulo { get; set; }

        [StringLength(250)]
        public virtual String Subtitulo { get; set; }

        [Required]
        public virtual String Autor { get; set; }

        public virtual String Descricao { get; set; }

        [StringLength(250)]
        public virtual String Editora { get; set; }

        public virtual String Isbn { get; set; }

        public virtual Int64 NumeroPaginas { get; set; }

        public virtual String AnoPublicacao { get; set; }

        public virtual String Categorias { get; set; }

        [StringLength(50)]
        public virtual String Idioma { get; set; }

        public virtual String SmallImage { get; set; }

        public virtual String MediumImage { get; set; }

        public virtual String LargeImage { get; set; }

        public virtual String ExtraLargeImage { get; set; }

        public virtual String SmallThumbnail { get; set; }

        public virtual String Thumbnail { get; set; }

        public string StatusLeitura { get; set; }

        public IList<NotaResponse> Notas { get; set; }
    }
}

