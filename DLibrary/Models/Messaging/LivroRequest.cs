﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DLibrary.Models
{
    public class LivroRequest
	{
        public virtual Int64 Id { get; set; }

        [Required, StringLength(250)]
        public virtual String Titulo { get; set; }

        [StringLength(250)]
        public virtual String Subtitulo { get; set; }

        [Required]
        public virtual String Autor { get; set; }

        [StringLength(500)]
        public virtual String Descricao { get; set; }

        [StringLength(250)]
        public virtual String Editora { get; set; }

        public virtual String Isbn { get; set; }

        public virtual Int64 NumeroPaginas { get; set; }

        public virtual String anoPublicacao { get; set; }

        public virtual String Categorias { get; set; }

        [StringLength(50)]
        public virtual String Idioma { get; set; }

        public virtual String SmallImage { get; set; }

        public virtual String MediumImage { get; set; }

        public virtual String LargeImage { get; set; }

        public virtual String ExtraLargeImage { get; set; }

        public virtual String SmallThumbnail { get; set; }

        public virtual String Thumbnail { get; set; }

        public virtual string StatusLeitura { get; set; }
    }
}
