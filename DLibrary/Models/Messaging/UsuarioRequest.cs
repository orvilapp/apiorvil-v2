﻿using System.ComponentModel.DataAnnotations;

namespace DLibrary.Models
{
    public class UsuarioRequest
	{
		public string UserName { get; set; }
        
		public string Password { get; set; }

        [Required]
        public string Email { get; set; }

        public string IdExterno { get; set; }

        public string TokenExterno { get; set; }

        public string Ativo { get; set; }
    }
}
