﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DLibrary.Models
{
    public class NotaRequest
	{
        [Required]
        public Int64 IdLivro { get; set; }

        [Required]
        public Int64 IdUsuario { get; set; }

        [Required]
        public String Titulo { get; set; }

        [Required]
        public String Cor { get; set; }

        [Required]
        public String Texto { get; set; }

        [Required]
        public Int64 Pagina { get; set; }

        [StringLength(50)]
        public String Tag { get; set; }

        public DateTime? DataCriacao { get; set; }
    }
}
