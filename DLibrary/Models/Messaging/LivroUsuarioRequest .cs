﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DLibrary.Models
{
    public class LivroUsuarioRequest
	{
        [Required]
        public virtual Int64 Id { get; set; }

        [Required]
        public virtual Int64 IdLivro { get; set; }

        [Required]
        public virtual Int64 IdUsuario { get; set; }
    }
}
