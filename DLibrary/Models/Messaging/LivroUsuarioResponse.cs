﻿using DLibrary.Data.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace DLibrary.Models
{
    public class LivroUsuarioResponse
	{
        [Required]
        public virtual Int64 Id { get; set; }

        [Required]
        public virtual Int64 IdLivro { get; set; }

        [Required]
        public virtual Int64 IdUsuario { get; set; }

        [Required]
        public virtual String Texto { get; set; }

        [Required]
        public virtual Int64 Pagina { get; set; }

        [StringLength(50)]
        public virtual String Tag { get; set; }
    }
}
