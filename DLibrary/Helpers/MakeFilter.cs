﻿using DLibrary.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Script.Serialization;

namespace DLibrary.Helpers
{
    public class MakeFilter
    {
        public static Expression<Func<T, bool>> Do<T>(String model, String JsonData, bool IsUserFilter)
        {
            var lista = new List<T>();

            JavaScriptSerializer oJS = new JavaScriptSerializer();
            var listaSearch = new List<SearchProperty>();

            if (!String.IsNullOrEmpty(JsonData))
                    listaSearch = oJS.Deserialize<List<SearchProperty>>(JsonData).ToList();
               
            if (String.IsNullOrEmpty(JsonData))
                return null;
            else
            {
                ParameterExpression parameter = Expression.Parameter(typeof(T));
                var members = new List<MemberEntity>();
                if (listaSearch.Count == 0)
                    return null;
                foreach (var i in listaSearch)
                {
                    var multipleProperties = i.Field.Split(',');

                    foreach (var innerPropertie in multipleProperties)
                    {
                        try
                        {
                            members.Add(new MemberEntity()
                            {
                                MemberExpression = Expression.Property(parameter, innerPropertie),
                                Value = i.Value,
                                Operator = i.Operator,
                                Condition = i.Condition
                            });
                        }
                        catch (Exception) { }
                    }
                }

                Expression predicate = null;
                foreach (var item in members)
                {
                    var n = 0;
                    ConstantExpression constant = int.TryParse(item.Value, out n) ? 
                        Expression.Constant(Convert.ToInt64(item.Value)) :
                        Expression.Constant(item.Value);

                    MethodInfo withMethod =  int.TryParse(item.Value, out n) ?
                        typeof(Int64).GetMethod(ReturnMethodSearch(item.Operator), BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(Int64) }, null) :
                        typeof(string).GetMethod(ReturnMethodSearch(item.Operator), BindingFlags.Public | BindingFlags.Instance, null, new Type[] { typeof(string) }, null); ;

                    MethodCallExpression callExp = Expression.Call(item.MemberExpression, withMethod, constant);
                    if (predicate == null)
                        predicate = (Expression)callExp;
                    else
                    {
                        if(item.Condition == "and")
                            predicate = Expression.And(predicate, callExp);
                        else if (item.Condition == "or")
                            predicate = Expression.Or(predicate, callExp);
                    }
                }

                if (predicate == null || parameter == null)
                    return null;

                return Expression.Lambda<Func<T, bool>>(predicate, parameter);
            }
        }

        public static String ReturnMethodSearch(String method)
        {
            if (method == "contains")
                return "Contains";
            else
                return "Equals";
        }
    }

    public class MemberEntity
    {
        public MemberExpression MemberExpression { get; set; }
        public String Value { get; set; }
        public String Operator { get; set; }
        public String Condition { get; set; }
    }
}