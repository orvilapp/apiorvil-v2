﻿using AutoMapper;
using DLibrary.Model.Models;
using DLibrary.Models;

namespace DLibrary.Mappers
{
    public class DomainToResponse : Profile
	{
		public DomainToResponse()
		{
            CreateMap<Livro, LivroResponse>();

            CreateMap<Nota, NotaResponse>();
		}
	}
}