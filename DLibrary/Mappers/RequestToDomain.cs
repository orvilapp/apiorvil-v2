﻿using AutoMapper;
using DLibrary.Data.Models;
using DLibrary.Model.Models;
using DLibrary.Models;

namespace DLibrary.Mappers
{
    public class RequestToDomain : Profile
	{
		public RequestToDomain()
		{
			CreateMap<LivroRequest, Livro>();
			CreateMap<NotaRequest, Nota>();
			CreateMap<UsuarioRequest, Usuario>();
		}
	}
}