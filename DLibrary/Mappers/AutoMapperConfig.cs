﻿using AutoMapper;

namespace DLibrary.Mappers
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DomainToResponse>();
                x.AddProfile<RequestToDomain>();
            });
        }
    }
}