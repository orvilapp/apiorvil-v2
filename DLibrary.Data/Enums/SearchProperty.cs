﻿using System;

namespace DLibrary.Data.Enums
{
    public class SearchProperty
    {
        public String Field { get; set; }
        public String Operator { get; set; }
        public String Value { get; set; }
        public String Condition { get; set; }
    }
}
