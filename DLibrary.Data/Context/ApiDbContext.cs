﻿using DLibrary.Data.Models;
using DLibrary.Model.Models;
using MySql.Data.Entity;
using System.Data.Entity;

namespace DLibrary.Data.Context
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
	public class ApiDbContext : DbContext
    {
        public ApiDbContext() : base("Name=Context")
        {

        }

        public DbSet<Livro> Livro { get; set; }
        public DbSet<Nota> Nota { get; set; }
        public DbSet<LivroUsuario> LivroUsuario { get; set; }
        public DbSet<Usuario> Usuario{ get; set; }

        ////protected override void OnModelCreating(DbModelBuilder modelBuilder)
        ////{
        ////    modelBuilder.Entity<Nota>()
        ////        .Property(e => e.Livro.Descricao).HasColumnName("LivroDescricao");
        ////}
    }
}
