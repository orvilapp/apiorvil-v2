﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DLibrary.Model.Models
{
    [Table(name: "livroUsuario", Schema = "whereit")]
    public class LivroUsuario
    {
        [Key]
        [Column("id_livroUsuario")]
        [Required]
        public virtual Int64 Id { get; set; }

        [ForeignKey("Livro")]
        [Column("id_livro")]
        [Required]
        public virtual Int64 IdLivro { get; set; }
        public virtual Livro Livro { get; set; }

        [Column("id_usuario")]
        [Required]
        public virtual Int64 IdUsuario { get; set; }

        [Column("data_criacao")]
        public virtual DateTime DataCriacao { get; set; }


        [Column("status_leitura")]
        [StringLength(10)]
        public virtual string StatusLeitura { get; set; }
    }
}
