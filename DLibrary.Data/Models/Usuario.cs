﻿using DLibrary.Model.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DLibrary.Data.Models
{
    [Table(name: "usuario", Schema = "whereit")]
    public class Usuario
    {
        [Key]
        [Column("id_usuario", Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public virtual Int64 Id { get; set; }

        [Column("nome")]
        [Required, StringLength(500)]
        public virtual String UserName { get; set; }

        [Column("email")]
        [Required, StringLength(100)]
        public virtual String Email { get; set; }

        [Column("senha")]
        [Required, StringLength(500)]
        public virtual String Password { get; set; }

        [Column("token_externo")]
        [StringLength(500)]
        public virtual String TokenExterno { get; set; }

        [Column("id_externo")]
        [StringLength(500)]
        public virtual String IdExterno { get; set; }

        [Column("url_foto")]
        [StringLength(500)]
        public virtual String UrlFoto { get; set; }

        [Column("ativo")]
        [StringLength(1)]
        public virtual String Ativo { get; set; }

        public virtual IList<Nota> Notas { get; set; }
    }
}
