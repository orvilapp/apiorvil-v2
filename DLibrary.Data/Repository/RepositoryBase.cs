﻿using DLibrary.Data.Context;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DLibrary.Data.Repository
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
	{
		internal ApiDbContext _context;
		internal DbSet<TEntity> DbSet;

		public RepositoryBase()
		{
			this._context = new ApiDbContext();
			this.DbSet = _context.Set<TEntity>();
		}

		public void Add(TEntity entity)
		{
			DbSet.Add(entity);
		}

		//public IEnumerable<TEntity> Get(Func<TEntity, bool> filter)
		//{
		//	return DbSet.Where(filter).ToList();
		//}

		public TEntity GetById(int id)
		{
			return DbSet.Find(id);
		}

		public IQueryable<TEntity> GetAll()
		{
			return DbSet;
		}

		public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
		{
			IQueryable<TEntity> query = DbSet;

			if (filter != null)
			{
				query = query.Where(filter);
			}

			if (orderBy != null)
			{
				return orderBy(query);
			}
			else
			{
				return query;
			}
		}

		public void Update(TEntity entity)
		{
			DbSet.Attach(entity);
			_context.Entry(entity).State = EntityState.Modified;
		}

		public void Delete(int id)
		{
			TEntity toDelete = DbSet.Find(id);
			Delete(toDelete);
		}

		public void Delete(TEntity entity)
		{
			if (_context.Entry(entity).State == EntityState.Detached)
			{
				DbSet.Attach(entity);
			}
			DbSet.Remove(entity);
		}

		public void Save()
		{
			_context.SaveChanges();
		}

		public void Dispose()
		{
			DbSet = null;
			_context.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}
